from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name='audino-bulk-upload',
    version='1.0.0',
    description='Script for uploading audio files to Audino',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/ycheremushkin/audino-bulk-upload',
    author='Yury Cheremushkin',
    author_email='cheyuriydev@gmail.com',
    packages=find_packages(),
    python_requires='>=3.6, <4',
    install_requires=["mysql-connector-python", "tqdm"],
    entry_points={ 
        'console_scripts': [
            'audino-bulk-upload=src:main',
        ],
    },
)