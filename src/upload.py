import argparse
import os
import shutil
from pathlib import Path
import mysql.connector as mysql
import itertools
from tqdm import tqdm

def args_parser():
    parser = argparse.ArgumentParser(description="Upload sample data to project")
    parser.add_argument("--db-host", "-db", type=str, help="host:port of db", required=True)
    parser.add_argument("--db-credentials", "-conn", type=str, help="username:password (or username if password is in env variable AUDINO_DB_PASSWORD) of db user", required=True)
    parser.add_argument("--source-dir", "-from", type=str, help="Directory with files", required=False, default="./")
    parser.add_argument("--data-dir", "-to", type=str, help="Directory to store files (binded with container's volume)", required=False, default="./")
    parser.add_argument("--project", "-p", type=int, help="Project id", required=True)
    parser.add_argument("--user", "-u", type=int, help="User id (all users in project if skipped)", required=False)
    parser.add_argument("--new-extension", type=str, help="Rename extension to", required=False)
    parser.add_argument('files', nargs=argparse.REMAINDER, help="Files to upload")         

    return parser

def get_users_in_project(conn, project_id):
    cursor = conn.cursor()
    cursor.execute("SELECT user_id FROM user_project WHERE project_id=%s", (project_id,))
    users_in_project = cursor.fetchall()  
    cursor.close()
    return list(itertools.chain(*users_in_project))

def insert_file_into_project(conn, project_id, user_id, filename):
    cursor = conn.cursor()
    row = {
        "project_id": project_id,
        "assigned_user_id": user_id,
        "filename": filename,
        "original_filename": filename,
        "is_marked_for_review": 0
    }
    cursor.execute("""
        INSERT INTO data (project_id, assigned_user_id, filename, original_filename, is_marked_for_review)
        VALUES (%(project_id)s, %(assigned_user_id)s, %(filename)s, %(original_filename)s, %(is_marked_for_review)s)
    """, row)
    conn.commit()
    cursor.close()

def main():
    args = vars(args_parser().parse_args())

    data_path = Path(args.get("data_dir"))
    print("Data path:", data_path)

    source_path = Path(args.get("source_dir"))
    print("Source path:", source_path)

    new_extension = args.get("new_extension")
    print("New extension:", new_extension)

    files = args.get("files")
    print("Specified files:", files)

    db_host, db_port = args.get("db_host").split(":")
    print("DB: {}:{}".format(db_host, db_port))

    credentials = args.get("db_credentials").split(":")
    db_user, db_password = credentials if len(credentials) > 1 else (credentials[0], os.getenv("AUDINO_DB_PASSWORD", None)) 
    print("DB credentials: {}:{}".format(db_user, "***"))

    project_id = args.get("project")
    print("Project ID:", project_id)

    conn = mysql.connect(user=db_user, password=db_password, host=db_host, port=db_port, database='audino')
    user_id = args.get("user")
    if user_id is None:
        print("User ID is not provided. Files will be assigned to all users in project.", end=" ")
        users = get_users_in_project(conn, project_id)
        print("Users ID:", users)
    else:
        users = [user_id]
        print("Users ID:", users)

    print("")

    files_to_add = []
    for file in tqdm(list(source_path.iterdir())):
        file_name = file.name
        if (len(files) > 0) and (file_name not in files):
            continue

        dest_file_name = file_name
        if new_extension is not None:
            dest_file_name = ".".join(file_name.split(".")[:-1])+new_extension

        if (data_path == source_path):
            shutil.move(source_path.joinpath(file_name), data_path.joinpath(dest_file_name))
        else:
            shutil.copy(source_path.joinpath(file_name), data_path.joinpath(dest_file_name))

        files_to_add += [dest_file_name]

    for file, user in tqdm(list(itertools.product(files_to_add, users))):
        insert_file_into_project(conn, project_id, user, file)
    
    print("Inserting data in DB")
    conn.close()
    print("Done!")